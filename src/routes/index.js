import Vue from 'vue'
import VueRouter from 'vue-router'
import Project from '@/pages/Project'
import User from '@/pages/User'

Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    {
      name: 'home',
      path: '/',
      component: Project
    },
    {
      name: 'project',
      path: '/project',
      component: Project
    },
    {
        name: 'user',
        path: '/user',
        component: User
    }
  ],
  mode: 'history'
})
