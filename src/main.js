import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './routes'
import axios from 'axios'

Vue.config.productionTip = false
axios.defaults.baseURL = 'http://localhost:5000'

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
